---
title: "Common Documents"
author: [Douglas Yu]
date: "2022-10-09"
...

## Passenger SAFETY Briefing {-#pax}

\noindent
![](chapters/common/pax-brief.pdf){width=100%}

## Hazardous Attitudes {-#HAT}

\noindent
![](chapters/common/Hazardous Attitudes.pdf){width=100%}

## Crosswind Chart {-#xwind}

\noindent
![](chapters/common/xwind.pdf){width=100%}

## Climb / Descent Rate {-#cdr}

\centering
![](chapters/common/climb-rate.pdf){width=95%}

\flushleft

## Holding Pattern {-#holding}

\centering
![](chapters/common/holding.pdf){width=80%}

\flushleft

## U.S. National Airspace System {-#NAS}

\centering
![](chapters/common/Airspacecard.pdf){width=100%}

![](chapters/common/Airspace2.pdf){width=100%}

\flushleft

## ICAO Flight Plan Form {-#ICAO}

\noindent
![](chapters/common/icao-fpl.pdf){width=100%}

## PIREP Form {-#pirep}

\  

\noindent
![](chapters/common/PIREP_FORM.pdf){width=80%}

\newpage

## Conversion Tables {-#conv}

\noindent
![](chapters/common/conv1.jpeg){width=100%}

\newpage

## IFR Clearance Components {-#craft}

\noindent
Most IFR clearances consist of five basic components (“*CRAFT*”):

- **C**learance limit: Your destination airport or an intermediate fix.
- **R**oute of flight: Hopefully the route you filed, unless traffic conditions dictate otherwise.
- **A**ltitude: If not as requested, typically followed by when to expect climb or descent clearance.
- **F**requency: The radio frequency for departure control.
- **T**ransponder: Your four-digit squawk code.

## IFR Position Report Components {-#pos}

\noindent
Include the following items when making a position report (“*IPATTEN*”):

- **I**dentification
- **P**osition
- **A**ltitude
- **T**ime
- **T**ype of flight plan*
- **E**TA to next reporting point
- **N**ame of next reporting point

* Not required in IFR position reports made directly to ATC centers or approach control.

\newpage

## Lost Communication Procedure {-#losscomm}

\noindent
![](chapters/common/lostcomm.pdf){width=100%}

## ATC Light Gun Signal {-#91.125}

\centering
![](chapters/common/lightgun.png){width=63%}
\flushleft


## Important Contact {-#impc}

\  

### Dispatch: {-#a}

### Flight Department: {-#b}

### Emergency Contact: {-#c}

### Flight Service: 800-WX-BRIEF {-#d}
