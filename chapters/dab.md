---
title: "Local Airport Diagram"
author: [Douglas Yu]
date: "2022-10-08"
...

## Airports Diagrams (Controlled) {-#adp}

\

\noindent
The following airport diagrams are included in this reference handbook.

\  

 - Daytona Beach International Airport (KDAB)

 - Ormond Beach Municipal Airport (KOMN)

 - Flagler Executive Airport (KFIN)

 - New Smyrna Beach Municipal Airport (KEVB)

 - Northeast Florida Regional Airport (KSGJ)

 - Orlando Sanford International Airport (KSFB)

\includepdf[noautoscale]{chapters/kdab/00110ad.pdf}

\includepdf[noautoscale]{chapters/kdab/05459ad.pdf}

\includepdf[noautoscale]{chapters/kdab/06440ad.pdf}

\includepdf[noautoscale]{chapters/kdab/06459ad.pdf}

\includepdf[noautoscale]{chapters/kdab/00692ad.pdf}

\includepdf[noautoscale]{chapters/kdab/00917ad.pdf}

## KSFB - Monroe / Jessup Arrival {-#KSFB}

\noindent
**Monroe Arrival**

Proceed to a point 5 NM due north of KSFB (northeast shore Lake Monroe),
then head southbound. Maintain 1,500 ft. MSL until advised by tower.

\noindent
**Jessup Arrival**

Proceed to a point 5 NM due south of KSFB (south shore Lake Jessup), then
head northbound. Maintain 1,500 ft. MSL until advised by tower.

\  

\noindent
![](chapters/kdab/ksfb.png){width=100%}

\newpage

## Airports Diagrams (Uncontrolled) {-#uadp}

\

\noindent
The following airport diagrams are included in this reference handbook.

 - DeLand Municipal Airport (KDED) 
 
 - Palatka Municipal Airport (28J)
 
 - Massey Ranch Airpark (X50)
 
\ 

\noindent
*All modified from ForeFlight, Note: may not current.*
 
\includepdf{chapters/kdab/KDED-FF-TaxiDiagram.pdf}

\includepdf{chapters/kdab/28J-FF-TaxiDiagram.pdf}

\includepdf{chapters/kdab/X50-FF-TaxiDiagram.pdf}

## Practice Area {-#PA}

\noindent
![](chapters/kdab/practice.pdf){width=95%}


## Restricted Areas {-#RA}

\noindent
**CHECK STATUS WITH JACKSONVILLE APPROACH PRIOR TO ENTRY**

118.60 MHz – 3,000 to 7,000 ft. MSL

128.675 MHz – above 7,000 ft. MSL

\noindent
**MONITOR SEALORD AT ALL TIMES ON 134.65 MHz**

\ 

\noindent
![](chapters/kdab/restrict-area.pdf){width=95%}
