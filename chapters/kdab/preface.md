---
title: "preface"
author: [intro]
date: "2022-10-11"
...

## Introduction {-#intro}

The *In-Cockpit Reference* was created by *Solar Flyer（迎晖航空）*，aiming to provide a quick, handy, and fully customized in-flight reference for local flight, local airport diagram, and other usable in-cockpit information.

### NOTE:{-#note}

**This Reference is not substitute for navigation charts, approach charts or other navigational publication. All user should have a current aeronautical publication for navigation.**

### Disclaimer {-#dis}

*All controlled airport charts are downloaded from the FAA website, and most of non-controlled airport charts are derived from the ForeFlight website. The author cannot guarantee that every chart is current*. **Please check the expiration date and/or AIRAC Cycle number of each chart carefully before use**.

\  

\  

\  

\  

\  

\  

\noindent
***Open Source License***

\noindent\footnotesize
The source code of the *In-Cockpit Reference* is licensed under BSD 3-Clause License.

\noindent\footnotesize
Copyright (c) 2022, Solar Flyer
All rights reserved.


\newpage

## Preflight Risk Assessment Matrix {-#FRAT}

\footnotesize
**Remember, as the pilot in command, you have the ultimate responsibility for the safety of your flight.** 

\footnotesize
Before each flight, fill in the appropriate element score in the Rating column and total these numbers to assess your overall flight risk

\  

\noindent
![](chapters/common/FRAT.pdf){width=100%}
