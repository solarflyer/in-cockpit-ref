#!/bin/bash

wget -c http://mirrors.ctan.org/fonts/fandol.zip 
wget -c https://github.com/adobe-fonts/source-serif-pro/releases/download/3.001R/source-serif-pro-3.001R.zip
unzip fandol.zip 
unzip source-serif-pro-3.001R.zip
mv fandol/*.otf /usr/share/fonts/opentype/
mv source-serif-pro-3.001R/OTF/*.otf /usr/share/fonts/opentype/
fc-cache -f -v
